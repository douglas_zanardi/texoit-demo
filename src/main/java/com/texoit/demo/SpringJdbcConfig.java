package com.texoit.demo;

import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.commons.lang3.SystemUtils;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
 

@Configuration
@ComponentScan("br.com.altis")
@EnableJpaRepositories
@EnableTransactionManagement
@EnableAutoConfiguration(exclude = { HibernateJpaAutoConfiguration.class})

public class SpringJdbcConfig {
	
	@PostConstruct
	public void init() {
	 
		System.out.println("init jdbc config");
		 
	}
	
	

	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();

		sessionFactory.setDataSource(h2DataSource());
		sessionFactory.setPackagesToScan(new String[] { "com.texoit.demo" });
		sessionFactory.setHibernateProperties(hibernateProperties());

		return sessionFactory;
	}
	
	@Bean
	@Autowired
	public SessionFactory sessionFactoryHibernate(LocalSessionFactoryBean bean) {
		return bean.getObject();
	}
	
	

	@Bean
	public DataSource h2DataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
	 
		String path = "C:/data/demo";
		 
		dataSource.setDriverClassName("org.h2.Driver");
		dataSource.setUrl("jdbc:h2:file:"+path );
		dataSource.setUsername("sa");
		dataSource.setPassword("sa");

		return dataSource;
	}

	@Bean
	public JdbcTemplate jdbcTemaplte() {
		return new JdbcTemplate(h2DataSource());
	}
	
	@Bean
	@Autowired
	public HibernateTemplate hibernateTemplate(LocalSessionFactoryBean bean) {
		return new HibernateTemplate(sessionFactoryHibernate(bean));
	}

	@Bean
	@Autowired
	public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager txManager = new HibernateTransactionManager();
		txManager.setSessionFactory(sessionFactory);
		return txManager;
	}
	 

	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	@Bean
	public PlatformTransactionManager transactionManager() {

		JpaTransactionManager txManager = new JpaTransactionManager();
		txManager.setEntityManagerFactory(entityManagerFactory());
		return txManager;
	}

	@Primary
	@Bean
	public EntityManagerFactory entityManagerFactory() {

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setGenerateDdl(true);

		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan(new String[] { "com.texoit.demo.entity","com.texoit.demo.repo"  });
		factory.setDataSource(h2DataSource());
		factory.setJpaProperties(hibernateProperties());
		factory.afterPropertiesSet();

		return factory.getObject();
	}
	
	@Bean
	public EntityManager entityManger () {
	    return entityManagerFactory().createEntityManager();
	}

	Properties hibernateProperties() {
		return new Properties() {

			private static final long serialVersionUID = 1L;
			{
				setProperty("hibernate.hbm2ddl.auto", "create-drop");
				setProperty("hibernate.show_sql", "false");
				setProperty("hibernate.format_sql", "false");
				setProperty("hibernate.c3p0.acquire_increment", "1");
				setProperty("hibernate.c3p0.idle_test_period", "10");
				setProperty("hibernate.c3p0.min_size", "5");
				setProperty("hibernate.c3p0.max_size", "10");
				setProperty("hibernate.c3p0.timeout", "50");
				setProperty("hibernate.c3p0.preferredTestQuery", "select 1");
				setProperty("hibernate.c3p0.testConnectionOnCheckout", "true");
			}
		};
	}
	
	public String getOperatingSystemSystemUtils() {
	    String os = SystemUtils.OS_NAME; 
	    return os;
	}
 
}