package com.texoit.demo.entity;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "movie")
public class Movie { 

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id ;
	private int year;
	private String title;
	private String studios;
	@ManyToMany(fetch=FetchType.EAGER)
	private Set<Producer> producers;
	
 	private String winner;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getStudios() {
		return studios;
	}
	public void setStudios(String studios) {
		this.studios = studios;
	}
	  
	public String getWinner() {
		return winner;
	}
	public void setWinner(String winner) {
		this.winner = winner;
	}
	public Set<Producer> getProducers() {
		return producers;
	}
	public void setProducers(Set<Producer> producers) {
		this.producers = producers;
	}
	
	public Movie() {
		// TODO Auto-generated constructor stub
	}
	
	public Movie(Long id) {
		this.id = id;
	}
	
	
}
