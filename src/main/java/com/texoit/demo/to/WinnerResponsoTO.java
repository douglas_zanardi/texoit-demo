package com.texoit.demo.to;

import java.util.ArrayList;
import java.util.List;

import com.texoit.demo.entity.Winners;

public class WinnerResponsoTO {

	private List<Winners> min = new ArrayList<Winners>();
	private List<Winners> max = new ArrayList<Winners>();
	
	public List<Winners> getMin() {
		return min;
	}
	public void setMin(List<Winners> min) {
		this.min = min;
	}
	public List<Winners> getMax() {
		return max;
	}
	public void setMax(List<Winners> max) {
		this.max = max;
	}
	
	 
}
