package com.texoit.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.texoit.demo.entity.Producer;

;

@Repository
public interface ProducerRepository extends JpaRepository<Producer, String> {

}
