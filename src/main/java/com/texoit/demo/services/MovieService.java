package com.texoit.demo.services;

import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.texoit.demo.MovielistApplication;
import com.texoit.demo.entity.Movie;
import com.texoit.demo.entity.Producer;
import com.texoit.demo.repo.MovieRepository;
import com.texoit.demo.repo.ProducerRepository;
import com.texoit.demo.util.FileUtils;

@Service
public class MovieService {

	@Autowired
	private MovieRepository movieRepository;
	@Autowired
	private ProducerRepository producerRepository;
	@Autowired
	private FileUtils utils;
	
	public List<Movie> list(){
		return movieRepository.findAll();
	}
	
	public void save(Movie movie){
		movieRepository.save(movie);
	}
	public void update(Movie movie){
		movieRepository.save(movie);
	}
	public void delete(Long id){
		movieRepository.delete(new Movie(id));
	}
	
	public Movie get(Long id) {
		return movieRepository.findById(id).get();
	}
	
	public void readCsvFile() {
		String path = MovielistApplication.URL_FILE;
		Scanner scanner = null;
		Movie movie = null;
		try {
			String[] columns = null;
			String[] producers = null;
			Producer producer = null;
			scanner = utils.openFileRead(path);
			scanner.nextLine(); ///Forçar pular cabeçalho
			
			while(scanner.hasNext()) {
				String line = utils.getNextLine(scanner);
				if(line != null ) {
					columns = line.split(";");
					producers = StringUtils.trimToEmpty(columns[3]).split(" and |,");
					movie = new Movie();
						movie.setYear(Integer.parseInt(columns[0]));
						movie.setProducers(new HashSet<>());
						for(String p : producers) {
							if(StringUtils.isNotEmpty(p)) {
								producer = new Producer(StringUtils.trimToEmpty(p));
								try { producerRepository.save(producer); } catch (Exception e) {
									e.printStackTrace();
								}
								movie.getProducers().add(producer);
							}
						}
						
						try {
							movie.setTitle(StringUtils.trimToEmpty(columns[1]));
							movie.setStudios(StringUtils.trimToEmpty(columns[2]));
							movie.setWinner(StringUtils.trimToEmpty(columns[4])); 
						}catch (ArrayIndexOutOfBoundsException e) { } 

					movieRepository.save(movie);
				}else {
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				utils.closeScanner(scanner);
			} catch (Exception e2) { }
		}
		
		
	}
	
}
