package com.texoit.demo.services;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.texoit.demo.entity.Winners;
import com.texoit.demo.to.WinnerResponsoTO;

@Service
public class WinnersService {
	
	private final static int MIN = 1;
	private final static int MAX = 2; 

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	public WinnerResponsoTO findWinnersMaxMin(){
		WinnerResponsoTO winnerResponsoTO = new WinnerResponsoTO();
		for(Winners w : findWinners(MIN)) {
			winnerResponsoTO.getMin().add(w);	 
		}
		for(Winners w : findWinners(MAX)) {
			winnerResponsoTO.getMax().add(w);	 
		}
		return winnerResponsoTO;
	}
	
	
	@SuppressWarnings("deprecation")
	public List<Winners> findWinners(int type) {
		List<Winners> winners = new ArrayList<Winners>();
		
		String sql = "  SELECT DISTINCT * "
				+ " FROM   (SELECT m1.producers_name AS name, "
				+ "               m1.year           AS year1, "
				+ "               m2.year           AS year2, "
				+ "               m2.year - m1.year AS years "
				+ "        FROM   (SELECT * "
				+ "                FROM   movie m "
				+ "                       INNER JOIN movie_producer mp "
				+ "                               ON ( m.id = mp.movie_id ) "
				+ "                WHERE  m.winner = 'yes') m1 "
				+ "               INNER JOIN (SELECT * "
				+ "                           FROM   movie m "
				+ "                                  INNER JOIN movie_producer mp "
				+ "                                          ON ( m.id = mp.movie_id )) m2 "
				+ "                       ON m2.id IN (SELECT m.id "
				+ "                                    FROM   movie m "
				+ "                                           INNER JOIN movie_producer mp "
				+ "                                                   ON ( m.id = mp.movie_id ) "
				+ "                                    WHERE  m.winner = 'yes' "
				+ "                                           AND mp.producers_name LIKE "
				+ "                                               m1.producers_name "
				+ "                                           AND m1.id <> m.id "
				+ "                                           AND m1.year <= m.year "
				+ "                                    ORDER  BY m.year ASC "
				+ "                                    LIMIT  1) "
				+ "        WHERE  m2.producers_name = m1.producers_name) aux "
				+ " WHERE  aux.years IN (SELECT aux.years "
				+ "                     FROM   (SELECT m1.producers_name AS name, "
				+ "                                    m1.year           AS year1, "
				+ "                                    m2.year           AS year2, "
				+ "                                    m2.year - m1.year AS years "
				+ "                             FROM   (SELECT * "
				+ "                                     FROM   movie m "
				+ "                                            INNER JOIN movie_producer mp "
				+ "                                                    ON ( m.id = mp.movie_id ) "
				+ "                                     WHERE  m.winner = 'yes') m1 "
				+ "                                    INNER JOIN (SELECT * "
				+ "                                                FROM   movie m "
				+ "                                                       INNER JOIN movie_producer "
				+ "                                                                  mp "
				+ "                                                               ON "
				+ "                                                       ( m.id = mp.movie_id )) "
				+ "                                               m2 "
				+ "                                            ON m2.id IN "
				+ "                                    (SELECT m.id "
				+ "                                     FROM   movie m "
				+ "                                               INNER JOIN movie_producer mp "
				+ "                                                       ON ( m.id = mp.movie_id ) "
				+ "                                                         WHERE  m.winner = 'yes' "
				+ "                                                                AND "
				+ "                                    mp.producers_name "
				+ "                                    LIKE "
				+ " m1.producers_name "
				+ " AND m1.id <> m.id "
				+ " AND m1.year <= m.year "
				+ " ORDER  BY m.year ASC "
				+ " LIMIT  1) "
				+ " WHERE  m2.producers_name = m1.producers_name) aux ";
 		
		String sqlOrder = "";
		if(type == MIN)
		     sqlOrder = sql  + "  order by aux.years asc limit 1)";
		else
			 sqlOrder = sql  + "  order by aux.years desc limit 1)";
		
		winners.addAll(jdbcTemplate.query(sqlOrder ,new Object[] { }, new RowMapper<Winners>(){
            public Winners mapRow(ResultSet rs, int rowNum) throws SQLException {
            	Winners winners = new Winners();
            	winners.setProducer(rs.getString("name"));
            	winners.setInterval(rs.getInt("years"));
            	winners.setPreviousWin(rs.getInt("year1"));
            	winners.setFollowingWin(rs.getInt("year2"));
            	return winners;
			}
		}));
		 
		return winners;
	} 
	
}
