package com.texoit.demo;

import java.io.File;
import java.text.ParseException;

import javax.swing.JOptionPane;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
 

@SpringBootApplication
public class MovielistApplication {

	public static String URL_FILE = "";
	
	public static void main(String[] args) throws ParseException {
		
	 
		String path = JOptionPane.showInputDialog("Digite a URL do arquivo a ser lido");
		File file = new File(path);
		if(StringUtils.isNotBlank(path) && file.exists() && !file.isDirectory()) {
			URL_FILE = path;
			SpringApplication.run(MovielistApplication.class, args);
		}else {
			JOptionPane.showMessageDialog(null, "Path inválido");
		}
		
	}

}
