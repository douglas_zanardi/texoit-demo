package com.texoit.demo.rest.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.texoit.demo.entity.Movie;
import com.texoit.demo.services.MovieService;
import com.texoit.demo.services.WinnersService;
import com.texoit.demo.to.WinnerResponsoTO;

@RestController 
public class MovielistResource {

	@Autowired
	private MovieService movieService;
	@Autowired
	private WinnersService winnersService;
	
	@GetMapping("/")
	private Map< String, String> index() {
		Map< String, String> map = new HashMap<String, String>();
		map.put("Winners", "http://localhost:8080/winners");
		map.put("Listar (GET)", "http://localhost:8080/movie");
		map.put("Adicionar (POST)", "http://localhost:8080/movie");
		map.put("Atualizar (PUT)", "http://localhost:8080/movie/{id}");
		map.put("Buscar (GET)", "http://localhost:8080/movie/{id}");
		map.put("Deletar (DEL)", "http://localhost:8080/movie/{id}");
		
		return map;
	}
	
	@GetMapping("/movie")
	private List<Movie> listar() {
		return movieService.list();
	}
	
	@GetMapping("/movie/{id}")
	private Movie buscar(@PathVariable Long id) {
		return movieService.get(id);
	}
	
	@GetMapping("/winners")
	private WinnerResponsoTO listarWinners() {
		return winnersService.findWinnersMaxMin();
	}
	 
	@PostMapping(value = "/movie")
	public Movie save(@RequestBody Movie entity) {
		movieService.save(entity);
		return entity;
	}
	
	
	@RequestMapping(value = "/movie/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable Long id) {
		movieService.delete(id);
	}

	@RequestMapping(value = "/movie/{id}", method = RequestMethod.PUT)
	public void update(@RequestBody Movie entity, @PathVariable Long id) {
		Movie movie = movieService.get(id);
		if(movie != null) {
			entity.setId(id);
			movieService.update(entity);
		}		
	}
	 	
}
