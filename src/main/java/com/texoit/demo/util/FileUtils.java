package com.texoit.demo.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import org.springframework.stereotype.Component;

@Component
public class FileUtils {

	private InputStream inputStream;

 	public Scanner openFileRead(String path) throws IOException {
		inputStream = null;
		Scanner sc = null;

		inputStream = new FileInputStream(path);
		sc = new Scanner(inputStream, "UTF-8");
		
		return sc;

	}

 	public String getNextLine(Scanner scanner) {
		if (scanner.hasNextLine()) {
			return scanner.nextLine();
		}
		return null;
	}

 	public void closeScanner(Scanner scanner) throws IOException {
		try {
			if (scanner.ioException() != null) {
				throw scanner.ioException();
			}
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
			if (scanner != null) {
				scanner.close();
			}
		}
	}

}
