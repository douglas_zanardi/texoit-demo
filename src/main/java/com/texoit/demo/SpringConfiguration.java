package com.texoit.demo;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.texoit.demo.services.MovieService;

@Configuration
@ComponentScan(basePackages= "com.texoit.demo")
public class SpringConfiguration {

	@Autowired
	private MovieService movieService;
	
	@PostConstruct
	private void init() {
		System.out.println("loading movielist...");
		movieService.readCsvFile();
	}

}
