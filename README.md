# TexoIT Demo

Projeto demonstração REST
## Instalação

Importar projeto no eclipse, realizar build maven, executar pela classe 
`MovielistApplication`


## Banco de dados
Realizar mudança de destino do banco H2 na classe  `SpringJdbcConfig` metodo `h2DataSource`

## Teste de integração

Todos os metodos implementados se encontram na pagina `http://localhost:8080` logo apos executar projeto.

    Buscar (GET)":"http://localhost:8080/movie/{id}
    Deletar (DEL)":"http://localhost:8080/movie/{id}
    Adicionar (POST)":"http://localhost:8080/movie
    Winners (GET)":"http://localhost:8080/winners -- SOLICITADO
    Listar (GET)":"http://localhost:8080/movie
    Atualizar (PUT)":"http://localhost:8080/movie/{id}"
  